package hk.com.novare.bootcamp21.springbootexercise1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RaulBulanonApplication {

	public static void main(String[] args) {
		SpringApplication.run(RaulBulanonApplication.class, args);
	}

}

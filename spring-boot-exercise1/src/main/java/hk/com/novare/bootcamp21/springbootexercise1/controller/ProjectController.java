package hk.com.novare.bootcamp21.springbootexercise1.controller;

import hk.com.novare.bootcamp21.springbootexercise1.model.Project;
import hk.com.novare.bootcamp21.springbootexercise1.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/emp")
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @GetMapping("/projects")
    public List<Project> getAllEmp(){
        return  projectService.getAll();
    }

    @PostMapping("/add")
    public String addEdd(@RequestBody Project proj){
        return projectService.add(proj);
    }

    @PutMapping("/edit")
    public String editEmp(@RequestBody Project proj){
        return projectService.edit(proj);
    }
    @DeleteMapping("/delete")
    public String deleteEmp(@RequestParam int id){
        return projectService.delete(id);
    }
}

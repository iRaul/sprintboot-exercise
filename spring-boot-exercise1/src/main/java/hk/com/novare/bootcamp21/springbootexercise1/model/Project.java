package hk.com.novare.bootcamp21.springbootexercise1.model;

import lombok.*;



@Data
public class Project {

    private int id;
    private String name;
    private String description;
    private String startDate;
    private String endDate;

}

package hk.com.novare.bootcamp21.springbootexercise1.repository;

import hk.com.novare.bootcamp21.springbootexercise1.model.Project;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class ProjectRepository {
    List<Project> project = new ArrayList<Project>();

    public List<Project> getAll(){
        return project;
    }
    public String add(Project proj){
        project.add(proj);
        return "SUCCESSFULLY ADDED";
    }

    public String edit(Project proj) {
        project.stream().filter(e -> e.getId() == proj.getId()).forEach(e ->{
            e.setDescription(proj.getDescription());
            e.setName(proj.getName());
            e.setEndDate(proj.getEndDate());
            e.setStartDate(proj.getStartDate());
        });
        return "SUCESSFULL UPDATE";
    }

    public String delete(int id) {
        project.remove(id-1);
        return "DELETED EMPLOYEE";
    }
}

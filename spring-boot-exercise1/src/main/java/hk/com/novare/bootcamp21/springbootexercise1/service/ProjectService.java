package hk.com.novare.bootcamp21.springbootexercise1.service;

import hk.com.novare.bootcamp21.springbootexercise1.model.Project;
import hk.com.novare.bootcamp21.springbootexercise1.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProjectService  {
    @Autowired
    ProjectRepository projectRepository;
    public List<Project> getAll(){
        return projectRepository.getAll();
    }

    public String add(Project project){
        return projectRepository.add(project);

    }

    public String edit(Project proj) {
        return projectRepository.edit(proj);
    }

    public String delete(int id) {
        return  projectRepository.delete(id);
    }
}
